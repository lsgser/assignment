<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a href="/" class="navbar-brand">{{config('app.name','Assignment')}}</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item"><a href="/" class="nav-link">Home</a></li>
      <li class="nav-item"><a href="/create" class="nav-link">Add Contact</a></li>
    </ul>
  </div>	
</nav> 
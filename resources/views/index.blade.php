@extends('layouts.app')

@section('content')
<a href="/create" class="btn btn-primary">Add Contacts</a>
<table class="table table-hover table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Surname</th>
      <th scope="col">Number</th>
      <th scope="col">Email</th>
      <th scope="col">Added on</th>

    </tr>
  </thead>
  <tbody>
  	@foreach($contacts as $contact)
    <tr>
      <th scope="row">{{$loop->iteration}}</th>
      <td>{{$contact->name}}</td>
      <td>{{$contact->surname}}</td>
      <td>{{$contact->numbers}}</td>
      <td>{{$contact->email}}</td>
      <td>{{$contact->created_at}}</td>
      <td><a href="/{{$contact->id}}/edit" class="btn btn-primary">Edit</a></td>
      <td>
      		{!! Form::open(['action'=>['ContactsController@destroy',$contact->id],'method' => 'POST']) !!}
				{{Form::hidden('_method','DELETE')}}
				{{Form::submit('Delete',['class'=>'btn btn-danger'])}}
			{!! Form::close() !!}
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
<!--This is for pagination-->
  {{$contacts->links()}}
@endsection
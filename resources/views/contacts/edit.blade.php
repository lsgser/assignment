@extends('layouts.app')

@section('content')
	<h1>Edit Contact</h1>
	{!! Form::open(['action' => ['ContactsController@update',$contact->id],'method'=>'POST']) !!}
		<div class="form-group">
			{{Form::label('name','Name:')}}
			{{Form::text('name',$contact->name,['class' => 'form-control','placeholder'=>'Name'])}}
			{{Form::text('surname',$contact->surname,['class' => 'form-control','placeholder'=>'Surname'])}}
			{{Form::text('email',$contact->email,['class' => 'form-control','placeholder'=>'Email'])}}
			{{Form::text('numbers',$contact->numbers,['class' => 'form-control','placeholder'=>''Contacts''])}}
		</div>
		{{Form::hidden('_method','PUT')}}
		{{Form::submit('Submit',['class'=>'btn btn-primary'])}}
	{!! Form::close() !!}
@endsection
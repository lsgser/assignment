@extends('layouts.app')

@section('content')
	<h1>Add Contact</h1>
	{!! Form::open(['action' => ['ContactsController@store'],'method'=>'POST']) !!}
		<div class="form-group">
			{{Form::label('name','Name:')}}
			{{Form::text('name','',['class' => 'form-control','placeholder'=>'Name'])}}
			{{Form::text('surname','',['class' => 'form-control','placeholder'=>'Surname'])}}
			{{Form::text('email','',['class' => 'form-control','placeholder'=>'Email'])}}
			{{Form::text('numbers','',['class' => 'form-control','placeholder'=>'Contacts'])}}
		</div>
		{{Form::submit('Submit',['class'=>'btn btn-primary'])}}
	{!! Form::close() !!}
@endsection
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get("/",'ContactsController@index');
Route::post("/",'ContactsController@store');
Route::get("/{id}/edit",'ContactsController@edit');
Route::put("/{id}",'ContactsController@update');
Route::get("/create",'ContactsController@create');
Route::delete("/{id}",'ContactsController@destroy');
Route::get("/{id}",'ContactsController@show');